FROM eclipse-temurin:21-jre-jammy
COPY dist /app
CMD /app/bin/app
