# Gradle GitLab CI/CD Components

[CI/CD components](https://docs.gitlab.com/ee/ci/components/) are reusable
single pipeline configuration units. This repository contains such units for
testing and building Gradle-based applications. They are used in projects
maintained by the IT department of the [University Library of
Basel](https://ub.unibas.ch).

## Components

### test

Runs unit tests and creates test reports

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `gradle_version` | Gradle version used in base image | `string` | `"8.8"` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `app_root` | Application root folder | `string` | `"."` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/gradle-components/test@main
    inputs:
      app_root: "./app"
```

#### Artifacts

- JUnit test reports in `reports-xml`


### test-coverage

Runs unit tests, optionally checks scalastyle rules and creates test reports

#### Requirements

- [JaCoCo Gradle Plugin](https://docs.gradle.org/current/userguide/jacoco_plugin.html) installed
- Tests must be run before the plugin. Make sure to have a task defined similar to this snippet:

```kotlin
tasks.named<Test>("test") {
    useJUnitPlatform()
}
```

- JaCoCo must produce CSV report:

```kotlin
// build.gradle.kts

tasks.jacocoTestReport {
    dependsOn(tasks.test) // tests are required to run before generating the report
    reports {
        csv.required = true // html reports are the default, so not explicitly enabled here
    }
}
```

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `stage` | Stage in CI/CD | `string` | `"test"` |
| `gradle_version` | Gradle version used in base image | `string` | `"8.8"` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `app_root` | Application root folder | `string` | `"."` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/gradle-components/test-coverage@main
    inputs:
      app_root: "./app"
```

#### Artifacts

- JoCoCo html test reports in `reports-html`


### build

Generates binaries and libraries for running the application

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `stage` | Stage in CI/CD | `string` | `"build"` |
| `gradle_version` | Gradle version used in base image | `string` | `"8.8"` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `app_root` | Application root folder | `string` | `"."` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/gradle-components/build@main
    inputs:
      app_root: "./app"
```

#### Artifacts

Binaries + libraries (in `dist/` dir) with expiration date +1h


### publish-lib

Creates and publishes a library 

#### Requirements

Target registry set up in Gradle configuration

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `stage` | Stage in CI/CD | `string` | `"publish"` |
| `gradle_version` | Gradle version used in base image | `string` | `"8.8"` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/gradle-components/publish-lib@main
    inputs:
      app_root: "./app"
```

#### Artifact

Published library


### pages

Creates and deploys Kotlin documentation as GitLab pages

#### Requirements

- [Dokka Gradle Plugin](https://kotlinlang.org/docs/dokka-introduction.html) installed

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `stage` | Stage in CI/CD | `string` | `"deploy"` |
| `gradle_version` | Gradle version used in base image | `string` | `"8.8"` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `app_root` | Application root folder | `string` | `"."` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/gradle-components/pages@main
    inputs:
      app_root: "."
```

#### Artifact

- Documentation in folder `public/`
- Documentation as [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/)


### pages-javadoc

Creates and deploys Java documentation as GitLab pages

#### Requirements

- `java` or `java-library` plugin activated

#### Inputs

| Inputs | Description | Value type | Default value |
| ------ | ----------- | ---------- | ------------- |
| `stage` | Stage in CI/CD | `string` | `"deploy"` |
| `gradle_version` | Gradle version used in base image | `string` | `"8.8"` |
| `additional_libs` | Additional libraries to installed in base image, separated by whitespace | `string` | `""` |
| `app_root` | Application root folder | `string` | `"."` |

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/gradle-components/pages-javadoc@main
    inputs:
      app_root: "."
```

#### Artifact

- Documentation in folder `public/`
- Documentation as [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/)


### gradle-default

Runs all components related to testing and building an application. Requires stages `test`, `build` and `deploy` per default.

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/gradle-components/gradle-default@main
```
